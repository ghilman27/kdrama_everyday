const BASE_URL = 'http://api.themoviedb.org/3';
const API_KEY = '59ed88bbeaf5870ffdbb096419f8412d';

class API {
	static async discover() {
		try {
			const url = `${BASE_URL}/discover/tv?api_key=${API_KEY}&with_original_language=ko&sort_by=popularity.desc&page=1`;
			const response = await fetch(url);
			const { results } = await response.json();
			return results;
		} catch (error) {
			return error;
		}
	}

	static async getDetail(id) {
		try {
			const url = `${BASE_URL}/tv/${id}?api_key=${API_KEY}`;
			const response = await fetch(url);
			const results = await response.json();
			return results;
		} catch (error) {
			return error;
		}
	}
}

export default API;
