import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Image,
	Dimensions,
	ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { useState, useEffect } from 'react';
import API from './api';

const vw = Dimensions.get('window').width;
const vh = Dimensions.get('window').height;

const DetailContent = (props) => {

	const imageUrl = props.data
		? `https://image.tmdb.org/t/p/w1000_and_h450_multi_faces${props.data.backdrop_path}`
		: `http://sjd.law.wfu.edu/files/2020/01/No-Photo-Available.jpg`;

	return (
		<View style={styles.detailContainer}>
			<Image source={{ uri: imageUrl }} style={styles.backdropImage} />
			<View style={styles.detailHeaderContainer}>
				<Text style={styles.dramaTitle}>{props.data.name}</Text>

				<View style={{ flexDirection: 'row'}}>
					<View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
						<Icon name='mode-comment' size={20} color={'white'} />
						<Text style={{fontSize: 22, color: 'white'}}> {props.data.popularity}</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
						<Icon name='star' size={22} color={'white'} style={{ marginLeft: 0.03 * vw }}/>
						<Text style={{fontSize: 22, color: 'white'}}> {props.data.vote_average}</Text>
					</View>
				</View>
			</View>

			<View style={styles.overviewContainer}>
				<Text style={styles.overviewHeader}>Overview</Text>
				<Text style={styles.overviewText}>{props.data.overview}</Text>
			</View>

			<View style={styles.factsContainer}>
				<Text style={styles.factsHeader}>Facts</Text>
				<Text style={styles.factsSection}>Original Name</Text>
				<Text style={styles.factsText}>{props.data.original_name}</Text>
				<Text style={styles.factsSection}>Status</Text>
				<Text style={styles.factsText}>{props.data.status}</Text>
				<Text style={styles.factsSection}>Number of Episodes</Text>
				<Text style={styles.factsText}>{props.data.number_of_episodes}</Text>
				<Text style={styles.factsSection}>Number of Seasons</Text>
				<Text style={styles.factsText}>{props.data.number_of_seasons}</Text>
				<Text style={styles.factsSection}>Type</Text>
				<Text style={styles.factsText}>{props.data.type}</Text>
				<Text style={styles.factsSection}>Original Language</Text>
				<Text style={styles.factsText}>{props.data.original_language}</Text>

			</View>
		</View>
	);
}

const DetailScreen = ({route}) => {
	const [data, setData] = useState(null);
	const [isLoading, setLoading] = useState(true);

	useEffect(() => {
		const fetchData = async () => {
			const data = await API.getDetail(route.params.id)
			setData(data);
			setLoading(false);
		};

		fetchData();
	}, []);

	if (isLoading) return <Text>Loading ....</Text>

	return (
		<ScrollView>
			<DetailContent data={data} />
		</ScrollView>
	);
};

const styles = StyleSheet.create({
	// Top level LoginScreen
	detailContainer: {
		flex: 1,
		width: vw,
	},
	backdropImage: {
		width: vw,
		height: 0.45 * vw,
		resizeMode: 'contain',
	},
	detailHeaderContainer: {
		// textAlign: 'center',
		// alignItems: 'center',
		fontSize: 24,
		backgroundColor: 'rgba(37,42,46, 0.95)',
		paddingVertical: 0.05 * vw,
	},
	dramaTitle: {
		fontSize: 24,
		marginBottom: 0.05 * vw,
		width: '100%',
		textAlign: 'center',
		color: 'white',
		fontWeight: '500'
	},
	overviewContainer: {
		padding: 0.06*vw,
		textAlign: 'left',
		backgroundColor: 'rgba(37,42,46, 0.94)',
	},
	overviewHeader: {
		marginBottom: 0.025 * vw,
		fontSize: 20,
		color: 'white',
		fontWeight: '500'
	},
	overviewText: {
		color: 'white'
	},
	factsContainer: {
		padding: 0.06*vw,
		textAlign: 'left',
	},
	factsHeader: {
		marginBottom: 0.05 * vw,
		fontSize: 20,
		fontWeight: '500'
	},
	factsSection: {
		fontWeight: 'bold'
	},
	factsText: {
		marginBottom: 0.025 * vw,
	}
});

export default DetailScreen;
