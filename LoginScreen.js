import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Image,
	TouchableOpacity,
	TextInput,
	Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const RememberCheckBox = () => {
	return (
		<View style={styles.rememberCheckBox}>
			<Icon
				style={{ marginRight: 5 }}
				name='check-box-outline-blank'
				size={20}
				color='#25A9E0'
			/>
			<Text style={{ color: '#8A8A8A' }}>Remember Me</Text>
		</View>
	);
};

const LoginButton = () => {
	return (
			<View style={styles.loginButtonBox}>
				<Text style={{ color: 'white' }}>LOGIN</Text>
			</View>
	);
};

const LoginInputBox = (props) => {
	return (
		<View style={styles.loginInputBox}>
			<Icon
				style={styles.loginInputIcon}
				name={props.name}
				size={20}
				color='#8A8A8A'
			/>
			<TextInput
				style={styles.loginInputText}
				placeholder={props.placeholder}
				underlineColorAndroid='transparent'
			/>
		</View>
	);
};

const LoginComponent = (props) => {
	return (
		<View style={props.style}>
			<LoginInputBox name='email' placeholder='Email' />
			<LoginInputBox name='lock' placeholder='Password' />
			<RememberCheckBox />
            <LoginButton />
		</View>
	);
};

const LoginScreen = ({ navigation }) => {
	return (
		<View style={styles.appContainer}>
			<Image source={require('./images/logo.png')} style={styles.appLogo} />
			<TouchableOpacity onPress={() => navigation.push('Home')}>
				<LoginComponent style={styles.loginComponentContainer} />
			</TouchableOpacity>
		</View>
	);
};

const vw = Dimensions.get('window').width;

const styles = StyleSheet.create({
	// Top level LoginScreen
	appContainer: {
		flex: 1,
		width: vw,
		alignItems: 'center',
		justifyContent: 'center',
	},
	appLogo: {
		width: 0.7 * vw,
		height: (0.7 * vw) / 3,
		resizeMode: 'contain',
		marginBottom: 0.05 * vw,
	},
	loginComponentContainer: {
		width: vw,
		height: 0.6 * vw,
		paddingHorizontal: '14%',
		marginBottom: 0.1 * vw,
	},
	loginInputBox: {
		flex: 1,
		borderRadius: 5,
		paddingLeft: 0.05 * vw,
		paddingVertical: 0.02 * vw,
		marginBottom: 0.03 * vw,
		flexDirection: 'row',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 2,
	},
	loginInputText: {
		height: '100%',
		marginLeft: '5%',
		color: '#8A8A8A',
		fontSize: 16,
	},
	loginInputIcon: {
		marginTop: '3%',
    },
    loginButtonBox: {
        backgroundColor: '#25A9E0',
        paddingVertical: 0.04 * vw,
        borderRadius: 5,
        marginTop: 0.07 * vw,
        alignItems: 'center'
    },
    rememberCheckBox: {
        flexDirection: 'row',
    }
});

export default LoginScreen;
