import React from 'react';
import { useState, useEffect } from 'react';
import {
	StyleSheet,
	Text,
	View,
	TextInput,
	Image,
	TouchableOpacity,
	Dimensions,
	ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import API from './api';
import { FlatList } from 'react-native-gesture-handler';
import { loaded, loading } from './action';
import { useSelector, useDispatch } from 'react-redux';

const colors = {
	deepskyblue: '#03a9f4',
	lightgray: '#ceced0',
};
const vw = Dimensions.get('window').width;
const vh = Dimensions.get('window').height;

const SearchBar = (props) => {
	const styles = StyleSheet.create({
		box: {
			flexDirection: 'row',
			alignItems: 'center',
			width: '100%',
			paddingVertical: 0.03 * vw,
			paddingLeft: 0.04 * vw,
			marginTop: 0.1 * vw,
			borderRadius: 20,
			borderWidth: 1,
			borderColor: colors.lightgray,
			elevation: 1,
		},
		textInput: {
			marginLeft: '5%',
			fontSize: 16,
		},
	});

	return (
		<View style={styles.box}>
			<Icon name='search' size={25} color={colors.deepskyblue} />
			<TextInput
				style={styles.textInput}
				placeholder={props.placeholder}
				underlineColorAndroid='transparent'
			/>
		</View>
	);
};

const DramaCard = (props) => {
	const styles = StyleSheet.create({
		dramaContainer: {
			flexDirection: 'row',
			width: 0.9 * vw,
			height: 0.9 * vw * 0.5,
			borderRadius: 10,
			overflow: 'hidden',
			elevation: 3,
			marginBottom: 0.03 * vh,
			backgroundColor: 'white',
		},
		dramaMedia: {
			flex: 1,
			height: '100%',
		},
		dramaContent: {
			flex: 1.5,
			paddingHorizontal: 0.05 * vw,
			paddingVertical: 0.05 * vw,
		},
		dramaTitle: {
			fontSize: 20,
			marginBottom: 0.02 * vw,
			fontWeight: '600',
		},
		dramaCaption: {
			flex: 1,
			justifyContent: 'flex-end',
		},
	});

	const imageUrl = props.data.poster_path ? `https://image.tmdb.org/t/p/w600_and_h900_bestv2/${props.data.poster_path}` : `http://sjd.law.wfu.edu/files/2020/01/No-Photo-Available.jpg`
	const id = props.data.id

	return (
		<TouchableOpacity onPress={() => props.openDetails(id)}>
			<View style={styles.dramaContainer}>
				<Image
					source={{uri: imageUrl}}
					style={styles.dramaMedia}
					resizeMode='contain'
				/>
				<View style={styles.dramaContent}>
					<View style={{ flex: 1 }}>
						<Text style={styles.dramaTitle}>{props.data.name}</Text>
						<View style={{ flexDirection: 'row', alignItems: 'center' }}>
							<Icon name='mode-comment' size={16} color={'black'} />
							<Text> {props.data.popularity}</Text>
							<Icon
								name='star'
								size={18}
								color={'black'}
								style={{ marginLeft: 0.03 * vw }}
							/>
							<Text> {props.data.vote_average}</Text>
						</View>
					</View>

					<View style={styles.dramaCaption}>
						<Text numberOfLines={2}>{props.data.overview}</Text>
					</View>
				</View>
			</View>
		</TouchableOpacity>
	);
};

const HomeScreen = ({ navigation }) => {
	// use redux
	const isLoading = useSelector(state => state.isLoading);
	const dispatch = useDispatch();

	// set loading and data still null
	const [data, setData] = useState(null);
	dispatch(loading);


	useEffect(() => {
		const fetchData = async () => {
			setData(await API.discover());
			dispatch(loaded);
		};

		fetchData();
	}, []);

	const styles = StyleSheet.create({
		container: {
			flex: 1,
			width: vw,
			alignItems: 'center',
			paddingHorizontal: 0.05 * vw,
		},
		contentWrapper: {
			flex: 1,
			marginVertical: 0.07 * vw,
			width: '100%',
		},
		sectionHeader: {
			fontSize: 24,
			fontWeight: '600',
			textAlign: 'left',
			marginBottom: 0.07 * vw,
			color: colors.deepskyblue,
		},
	});

	const openDetails = (id) => {
		navigation.navigate('Details', {id: id})
	}

	if (isLoading) return <Text>Loading....</Text>

	return (
		<ScrollView>
			<View style={styles.container}>
				<SearchBar placeholder='Hospital Playlist' />
				<View style={styles.contentWrapper}>
					<Text style={styles.sectionHeader}>Trending Dramas</Text>
					<FlatList
						data={data}
						renderItem={(drama) => <DramaCard data={drama.item} openDetails={openDetails}/>}
						keyExtractor={(item) => item.id.toString()}
						numColumns={1}
					/>
				</View>
			</View>
		</ScrollView>
	);
};

export default HomeScreen;
