import 'react-native-gesture-handler';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import HomeScreen from './HomeScreen';
import DetailScreen from './DetailScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import isLoading from "./reducer";

const store = createStore(
  isLoading,
  applyMiddleware(thunk)
);

store.subscribe(() => console.log(store.getState()));

const Tabs = createBottomTabNavigator();
const AppStack = createStackNavigator();
const Home = createStackNavigator();
const About = createStackNavigator();

const HomeStacks = () => {
  return (
    <Home.Navigator>
      <Home.Screen name='Home' component={HomeScreen} options={{ headerShown: false }} />
      <Home.Screen name='Details' component={DetailScreen} />
    </Home.Navigator>
  );
}

const AboutStack = () => {
  return (
    <About.Navigator>
      <About.Screen name='Profile' component={AboutScreen} options={{ headerShown: false }} />
    </About.Navigator>
  )
}

const HomeTabs = () => {
  return (
    <Tabs.Navigator>
      <Tabs.Screen name='Home' component={HomeStacks} options={{ headerShown: false }}/>
      <Tabs.Screen name='Profile' component={AboutStack} options={{ headerShown: false }}/>
    </Tabs.Navigator>
  );
};

export default function App() {
	return (
    <Provider store={store}>
      <NavigationContainer>
        <AppStack.Navigator initialRouteName='Login'>
          <AppStack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
          <AppStack.Screen name='Home' component={HomeTabs} options={{ headerShown: false }}/>
        </AppStack.Navigator>
      </NavigationContainer>
    </Provider>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});
