import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Image,
	TouchableOpacity,
	Dimensions,
} from 'react-native';

const appColor = {
	blue: '#25A9E0',
	darkBlue: '#003366',
	black: 'black',
	grey: '#757575',
	borderGrey: '#D5D9E1',
};

class ContactBox extends React.Component {
	render() {
		return (
			<TouchableOpacity>
				<View style={styles.contactBox}>
					<Image style={styles.contactIcon} source={this.props.imageUrl} />
					<Text style={styles.contactAccName}>{this.props.accountName}</Text>
				</View>
			</TouchableOpacity>
		);
	}
}

// Main Page Container
export default class AboutScreen extends React.Component {
	render() {
		return (
			<View style={styles.appContainer}>
					<View style={styles.profileBox}>
						<TouchableOpacity>
							<Image
								style={styles.profilePicture}
								source={require('./images/profile_photo.png')}
							/>
						</TouchableOpacity>
						<Text style={styles.profileName}>Ghilman Al Fatih</Text>

						<Text style={styles.headerName}>You can reach me in ...</Text>
						<View style={styles.contactContainer}>
							<ContactBox
								imageUrl={require('./images/instagram.jpg')}
								accountName='@ghilman27'
							/>
							<ContactBox
								imageUrl={require('./images/twitter.png')}
								accountName='@ghilaman27'
							/>
						</View>
						<View style={styles.contactContainer}>
							<ContactBox
								imageUrl={require('./images/facebook.png')}
								accountName='Ghilman Al Fatih'
							/>
						</View>

						<Text style={styles.headerName}>Find my project in ...</Text>
						<View style={styles.contactContainer}>
							<ContactBox
								imageUrl={require('./images/github.png')}
								accountName='@ghilman27'
							/>
						</View>
					</View>
			</View>
		);
	}
}

const vw = Dimensions.get('window').width;
const vh = Dimensions.get('window').height;

const styles = StyleSheet.create({
	// AboutScreen
	appContainer: {
        flex: 1,
        width: vw,
        height: vh,
		backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        paddingHorizontal: 0.1*vw,
        paddingVertical: 0.1*vh,
	},

	// Profile
	profileBox: {
		flex: 1,
		padding: 20,
		shadowColor: '#000000',
		shadowOpacity: 0.1,
		shadowRadius: 4,
		shadowOffset: {
			height: 1,
			width: 1,
		},
        alignItems: 'center',
        width: '100%',
        height: '100%'
	},
	photoBox: {
		width: 100,
		height: 100,
		borderRadius: 50,
		overflow: 'hidden',
		backgroundColor: 'red',
	},
	profilePicture: {
		resizeMode: 'contain',
		width: 100,
		height: 100,
		marginBottom: 5,
	},
	profileName: {
		color: appColor.darkBlue,
		fontSize: 18,
	},
	contactContainer: {
		flexDirection: 'row',
	},
	contactBox: {
		flexDirection: 'row',
		alignItems: 'center',
		borderWidth: 1,
		borderColor: appColor.borderGrey,
		borderRadius: 5,
		padding: 5,
		marginRight: 10,
		marginBottom: 10,
	},
	contactIcon: {
		resizeMode: 'contain',
		width: 15,
		height: 15,
		marginRight: 5,
	},
	contactAccName: {
		color: appColor.black,
		fontSize: 12,
	},
	headerName: {
		color: appColor.grey,
		marginBottom: 10,
		fontSize: 16,
		marginTop: 30,
	},
});
