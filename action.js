export const loaded = () => {
    return {
        type: 'LOADED'
    }
}

export const loading = () => {
    return {
        type: 'LOADING'
    }
}
